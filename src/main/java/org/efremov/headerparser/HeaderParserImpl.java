/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.headerparser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 *
 * @author efrem
 */
@ApplicationScoped
public class HeaderParserImpl implements HeaderParser {

    @Inject
    @OSRegex
    Pattern pOS;

    @Override
    public String getOSFromUserAgent(String input) {
        Matcher m = pOS.matcher(input);
        if (m.find()) {
            return m.group();
        }
        return null;
    }

}
