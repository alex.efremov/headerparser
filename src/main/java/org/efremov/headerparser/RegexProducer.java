/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.headerparser;

import java.util.regex.Pattern;
import javax.enterprise.inject.Produces;

/**
 *
 * @author efrem
 */
public class RegexProducer {
    @Produces
    @OSRegex
    private Pattern pOS = Pattern.compile("(?<=\\().*?(?=\\))");
}
