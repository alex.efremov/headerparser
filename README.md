Project for freeCodeCamp.

User Story: I can get the IP address, language and operating system for my browser.

To run this microservice you need docker and maven.


1. Package application running *mvn package*

2. Launch docker container *docker run -d -p 8080:8080 imageid*

3. Open http://localhost:8080