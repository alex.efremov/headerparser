# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
FROM openjdk:8
ADD target/headerparser-hollow-swarm.jar /opt/headerparser-hollow-swarm.jar
ADD target/headerparser.war /opt/headerparser.war
EXPOSE 8080
ENV JAVA_TOOL_OPTIONS="-Djava.net.preferIPv4Stack=true"
ENTRYPOINT ["java", "-jar", "/opt/headerparser-hollow-swarm.jar", "/opt/headerparser.war"]
